<!DOCTYPE html>
<html>
<head>
  <style>
  h1.oldschool {
    display: block;
    font-size: 2em;
    -webkit-margin-before: 0.67em;
    -webkit-margin-after: 0.67em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    font-weight: bold;
    font-family: serif;
    margin: 0;
  }
  </style>
</head>
<body>
<h1 class="oldschool">Index of /</h1>

<?php
echo '<ul>';

if($dossier = opendir('.')) {
  while(false !== ($fichier = readdir($dossier))) {

    if($fichier != '.' && $fichier != '..' && $fichier != 'index.php' && $fichier != '.DS_Store') {
      echo '<li><a href="./' . $fichier . '">' . $fichier . '</a></li>';
    } // On ferme le if (qui permet de ne pas afficher index.php, etc.)

  } // On termine la boucle

echo '</ul>';
closedir($dossier);
}

else {
     echo 'Le dossier n\' a pas pu être ouvert';
}
?>
<?php phpinfo(); ?>
</body>
</html>
